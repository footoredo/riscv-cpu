CC = iverilog
VVP = vvp

SRC = ./src
CPU = $(SRC)/cpu
BIN = ./bin
TB = ./testbench
SIM = ./sim
GEN = ./gen

CPU_V = $(CPU)/defines.v \
		$(CPU)/pc_reg.v \
		$(CPU)/regfile.v \
		$(CPU)/if_id.v \
		$(CPU)/id.v \
		$(CPU)/id_ex.v \
		$(CPU)/ex.v \
		$(CPU)/ex_mem.v \
		$(CPU)/mem.v \
		$(CPU)/mem_wb.v \
		$(CPU)/cpu.v \
		$(CPU)/if.v \
		$(CPU)/sopc.v \
		$(CPU)/ram.v \
		$(CPU)/ctrl.v

TB_V = $(TB)/tb.v

DATA = ./data/memory.data

tb: $(BIN)/tb $(SIM)/tb.lxt

$(BIN)/tb: $(CPU_V) $(TB_V) $(DATA)
	$(CC) -o $(BIN)/tb $(CPU_V) $(TB_V)

$(SIM)/tb.lxt: $(SIM)/tb.vvp
	$(VVP) $(SIM)/tb.vvp -lxt2

$(SIM)/tb.vvp: $(CPU_V) $(TB_V) $(DATA)
	$(CC) -o $(SIM)/tb.vvp $(CPU_V) $(TB_V)

$(DATA): $(GEN)/test.c
	cd $(GEN) && make && cd .. && cp $(GEN)/test.data $(DATA)

clean:
	find $(BIN)/ -mindepth 1 -delete && find $(SIM)/ -mindepth 1 -delete
