module pipeline_IF_test;
    reg RST = 0;
    initial begin
        $dumpfile ("pipeline_IF_test.vcd");
        $dumpvars (0, pipeline_IF_test);
        #1 RST = 1;
        #10 RST = 0;
        #10 RST = 1;
        #10 RST = 0;
        #20 $finish;
    end

    reg CLK = 0;
    always #1 CLK = !CLK;

    reg input_busy, ID_busy;
    wire fetch_inst_flag;
    wire [31:0] inst_addr;
    reg data_ready = 0;
    reg [31:0] data;
    reg jump_flag;
    reg [31:0] jump_addr;
    wire [31:0] inst, inst_PC;
    wire ready;

    always @ ( posedge RST ) begin
        input_busy <= 0;
        ID_busy <= 0;
        data_ready <= 0;
        jump_flag <= 0;
    end

    pipeline_IF IF (CLK, RST, input_busy, ID_busy,
        fetch_inst_flag, inst_addr,
        data_ready, data, jump_flag, jump_addr,
        inst, inst_PC, ready);

    always @ ( posedge ready ) begin
        $display ("At time %t, fetched inst %0d with addr %h.",
            $time, inst, inst_PC);
    end

    integer counter = 0;

    always @ ( posedge fetch_inst_flag ) begin
        $display ("At time %t, requesting to fetch inst with addr %h. %h",
            $time, inst_addr, fetch_inst_flag);
        data_ready <= 0;
        input_busy <= 1;
        #4 data <= counter;
        counter <= counter + 1;
        input_busy <= 0;
        data_ready <= 1;
    end
endmodule
