`timescale 1ns / 1ps

module tb ();
    reg CLOCK_50;
    reg rst;

    initial begin
        // $display("asda");
        CLOCK_50 = 1'b0;
        forever #10 CLOCK_50 = ~CLOCK_50;
    end

    initial begin
        rst = 1;
        #195 rst = 0;
        // #20000 $finish;
    end

    initial begin
        $dumpfile("sim/tb.lxt");
        $dumpvars(0, tb);
    end

    // for debugging
    reg req;
    reg [4:0] req_addr;
    wire [31:0] req_data;
    wire finished;

    sopc sopc0 (
            .clk(CLOCK_50),
            .rst(rst),

            .req(req), .req_addr(req_addr),
            .req_data(req_data),

            .finished(finished)
        );

    initial begin
        req <= 0;
    end

/*    integer i;
    integer file;
    always @ ( * ) begin
        if (finished) begin
            // $display("asda");
            #120 req <= 1;
            for (i = 0; i < 32; ++ i)
            begin
                #5 req_addr <= i;
                #5 $display("%2d %h", i, req_data);
            end
            #2000 $finish;
        end
        // #20000 $finish;
    end*/

    /*initial begin
        #50000 $finish;
    end*/

endmodule // tb
