module sopc (
        input wire clk,
        input wire rst,

        // for debugging
        input wire req,
        input wire [4:0] req_addr,
        output wire [31:0] req_data,
        output wire finished
    );

    wire [31:0] inst_addr;
    wire [31:0] inst;
    wire rom_ce;

    wire [31:0] ram_mem_addr;
    wire ram_mem_we;
    wire [3:0] ram_mem_mask;
    wire [31:0] ram_mem_data;
    wire ram_mem_ce;
    wire [31:0] ram_mem_data_o;
    wire ram_mem_done;

    wire [31:0] ram_inst_addr;
    wire ram_inst_ce;
    wire [31:0] ram_inst_data_o;
    wire ram_inst_done;

    cpu cpu0 (
            .clk(clk), .rst(rst),
            .ram_inst_addr_o(ram_inst_addr), .ram_inst_data_i(ram_inst_data_o),
            .ram_inst_ce_o(ram_inst_ce), .ram_inst_done_i(ram_inst_done),

            .req(req), .req_addr(req_addr),
            .req_data(req_data),

            .ram_mem_data_i(ram_mem_data_o),
            .ram_mem_addr_o(ram_mem_addr), .ram_mem_we_o(ram_mem_we),
            .ram_mem_mask_o(ram_mem_mask), .ram_mem_data_o(ram_mem_data),
            .ram_mem_ce_o(ram_mem_ce), .ram_mem_done_i(ram_mem_done)
        );

    ram_processor ram_processor0 (
            .clk(clk), .rst(rst),

            .mem_ce(ram_mem_ce), .mem_we(ram_mem_we),
            .mem_addr(ram_mem_addr), .mem_data_i(ram_mem_data),
            .mem_mask(ram_mem_mask),
            .mem_data_o(ram_mem_data_o),
            .mem_done(ram_mem_done),

            .inst_ce(ram_inst_ce),
            .inst_addr(ram_inst_addr),
            .inst_data(ram_inst_data_o),
            .inst_done(ram_inst_done),

            .finished(finished)
        );

endmodule // sopc
