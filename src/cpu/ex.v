module ex (
        input wire rst,

        input wire [`ALU_OP_BUS] aluop_i,
        input wire [`ALU_SEL_BUS] alusel_i,
        input wire [31:0] reg1_i,
        input wire [31:0] reg2_i,
        input wire [31:0] wdata_i,
        input wire [4:0] wd_i,
        input wire wreg_i,

        input wire [31:0] mem_offset,

        output reg [4:0] wd_o,
        output reg wreg_o,
        output reg [31:0] wdata_o
    );

    reg [31:0] logicout;

    always @ ( * ) begin
        if (rst) begin
            logicout <= 0;
        end else begin
            case (aluop_i)
                `ALU_OP_OR: logicout <= reg1_i | reg2_i;
                `ALU_OP_AND: logicout <= reg1_i & reg2_i;
                `ALU_OP_XOR: logicout <= reg1_i ^ reg2_i;
                `ALU_OP_SLL: logicout <= reg1_i << reg2_i[4:0];
                `ALU_OP_SRL: logicout <= reg1_i >> reg2_i[4:0];
                default: logicout <= 0;
            endcase
        end
    end

    reg [31:0] arithout;

    always @ ( * ) begin
        if (rst) begin
            arithout <= 0;
        end else begin
            case (aluop_i)
                `ALU_OP_ADD: arithout <= $signed(reg1_i) + $signed(reg2_i);
                `ALU_OP_SLT: arithout <= $signed(reg1_i) < $signed(reg2_i);
                `ALU_OP_SLTU: arithout <= reg1_i < reg2_i;
                `ALU_OP_SUB: arithout <= $signed(reg1_i) - $signed(reg2_i);
                `ALU_OP_SRA: arithout <= $signed(reg1_i) >>> reg2_i[4:0];
                `ALU_OP_ADD_MEM: arithout <= $signed(reg1_i) + $signed(mem_offset);
                default: arithout <= 0;
            endcase
        end
    end

    always @ ( * ) begin
        wd_o <= wd_i;
        wreg_o <= wreg_i;
        case (alusel_i)
            `ALU_SEL_LOGIC:
                wdata_o <= logicout;
            `ALU_SEL_ARITH:
                wdata_o <= arithout;
            default:
                wdata_o <= wdata_i;
        endcase
    end

endmodule
