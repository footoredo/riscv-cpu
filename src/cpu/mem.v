module mem (
        input wire rst,

        input wire [4:0] wd_i,
        input wire wreg_i,
        input wire [31:0] wdata_i,

        output reg [4:0] wd_o,
        output reg wreg_o,
        output reg [31:0] wdata_o,

        input wire [`MEM_OP_BUS] op_i,
        input wire [31:0] src_i,
        input wire [`MEM_SEL_BUS] sel_i,

        input wire [31:0] ram_data_i,

        output reg [31:0] ram_addr_o,
        output reg ram_we_o,            // 0 for read
        output reg [3:0] ram_mask_o,
        output reg [31:0] ram_data_o,
        output reg ram_ce_o,

        input wire ram_done,

        output reg stallreq
    );

    wire [31:0] addr;
    assign addr = wdata_i;

    task calc_mask_B;
        case (addr[1:0])
            2'b00: begin
                ram_mask_o <= 4'b1000;
            end
            2'b01: begin
                ram_mask_o <= 4'b0100;
            end
            2'b10: begin
                ram_mask_o <= 4'b0010;
            end
            2'b11: begin
                ram_mask_o <= 4'b0001;
            end
        endcase
    endtask

    task calc_mask_H;
        case (addr[1:0])
            2'b00: begin
                ram_mask_o <= 4'b1100;
            end
            2'b10: begin
                ram_mask_o <= 4'b0011;
            end
        endcase
    endtask

    task calc_mask_W;
        ram_mask_o <= 4'b1111;
    endtask

    always @ ( * ) begin
        if (rst) begin
            wd_o <= 0;
            wreg_o <= 0;
            wdata_o <= 0;

            ram_addr_o <= 0;
            ram_we_o <= 0;
            ram_mask_o <= 0;
            ram_data_o <= 0;
            ram_ce_o <= 0;

            stallreq <= 0;
        end else begin
            wd_o <= wd_i;
            wreg_o <= wreg_i;
            wdata_o <= wdata_i;

            ram_addr_o <= 0;
            ram_we_o <= 0;
            ram_mask_o <= 0;
            ram_data_o <= 0;
            ram_ce_o <= 0;

            case (sel_i)
                `MEM_SEL_LOAD: begin
                    ram_we_o <= 0;
                    ram_ce_o <= 1;
                    ram_addr_o <= addr;
                    case (op_i)
                        `MEM_OP_LB: begin
                            calc_mask_B;
                            case (addr[1:0])
                                2'b00: begin
                                    wdata_o <= {{24{ram_data_i[31]}}, ram_data_i[31:24]};
                                end
                                2'b01: begin
                                    wdata_o <= {{24{ram_data_i[23]}}, ram_data_i[23:16]};
                                end
                                2'b10: begin
                                    wdata_o <= {{24{ram_data_i[15]}}, ram_data_i[15:8]};
                                end
                                2'b11: begin
                                    wdata_o <= {{24{ram_data_i[7]}}, ram_data_i[7:0]};
                                end
                                default:
                                    wdata_o <= 0;
                            endcase
                        end
                        `MEM_OP_LBU: begin
                            calc_mask_B;
                            case (addr[1:0])
                                2'b00: begin
                                    wdata_o <= {{24{1'b0}}, ram_data_i[31:24]};
                                end
                                2'b01: begin
                                    wdata_o <= {{24{1'b0}}, ram_data_i[23:16]};
                                end
                                2'b10: begin
                                    wdata_o <= {{24{1'b0}}, ram_data_i[15:8]};
                                end
                                2'b11: begin
                                    wdata_o <= {{24{1'b0}}, ram_data_i[7:0]};
                                end
                                default:
                                    wdata_o <= 0;
                            endcase
                        end
                        `MEM_OP_LH: begin
                            calc_mask_H;
                            case (addr[1:0])
                                2'b00: begin
                                    wdata_o <= {{24{ram_data_i[31]}}, ram_data_i[31:16]};
                                end
                                2'b10: begin
                                    wdata_o <= {{24{ram_data_i[15]}}, ram_data_i[15:8]};
                                end
                                default:
                                    wdata_o <= 0;
                            endcase
                        end
                        `MEM_OP_LHU: begin
                            calc_mask_H;
                            case (addr[1:0])
                                2'b00: begin
                                    wdata_o <= {{24{1'b0}}, ram_data_i[31:16]};
                                end
                                2'b10: begin
                                    wdata_o <= {{24{1'b0}}, ram_data_i[15:8]};
                                end
                                default:
                                    wdata_o <= 0;
                            endcase
                        end
                        `MEM_OP_LW: begin
                            wdata_o <= ram_data_i;
                            calc_mask_W;
                        end
                    endcase
                end

                `MEM_SEL_STORE: begin
                    ram_we_o <= 1;
                    ram_ce_o <= 1;
                    ram_addr_o <= addr;
                    case (op_i)
                        `MEM_OP_SB: begin
                            ram_data_o <= {src_i[7:0], src_i[7:0], src_i[7:0], src_i[7:0]};
                            calc_mask_B;
                        end
                        `MEM_OP_SH: begin
                            ram_data_o <= {src_i[15:0], src_i[15:0]};
                            calc_mask_H;
                        end
                        `MEM_OP_SW: begin
                            ram_data_o <= src_i;
                            calc_mask_W;
                        end
                    endcase
                end

            endcase
        end
    end

    always @ ( * ) begin
        if (ram_ce_o && !ram_done)
            stallreq <= 1;
        if (ram_ce_o && ram_done) begin
            stallreq <= 0;
            ram_ce_o <= 0;
        end
    end

endmodule // mem
