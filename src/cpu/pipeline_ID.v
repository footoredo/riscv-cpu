`include "const.h"

module pipeline_ID (
        input CLK,
        input RST,

        input [31:0] inst,
        input [31:0] inst_PC,
        input inst_ready,

        output reg jump_flag,
        output reg [31:0] jump_PC,

        input EX_busy,

        output busy
    );

    always @ ( posedge RST ) begin
        if (RST) begin
            jump_flag <= 0;
            busy <= 0;
        end
    end

    function [31:0] imm_I;
        input [31:0] inst;
        interger i;
        begin
            for (i = 31; i >= 11; i = i - 1)
                imm_I[i] = inst[31];
            imm_I[10:5] = inst[30:25];
            imm_I[4:1] = inst[24:21];
            imm_I[0] = inst[20];
        end
    endtask

    function [31:0] imm_S;
        input [31:0] inst;
        interger i;
        begin
            for (i = 31; i >= 11; i = i - 1)
                imm_S[i] = inst[31];
            imm_S[10:5] = inst[30:25];
            imm_S[4:1] = inst[11:8];
            imm_S[0] = inst[7];
        end
    endtask

    function [31:0] imm_B;
        input [31:0] inst;
        interger i;
        begin
            for (i = 31; i >= 12; i = i - 1)
                imm_B[i] = inst[31];
            imm_B[11] = inst[7];
            imm_B[10:5] = inst[30:25];
            imm_B[4:1] = inst[11:8];
            imm_B[0] = 0;
        end
    endtask

    function [31:0] imm_U;
        input [31:0] inst;
        interger i;
        begin
            imm_U[31] = inst[31];
            imm_U[30:20] = inst[30:20];
            imm_U[19:12] = inst[19:12];
            for (i = 11; i >= 0; i = i - 1)
                imm_U[i] = 0;
        end
    endtask

    function [31:0] imm_J;
        input [31:0] inst;
        interger i;
        begin
            for (i = 31; i >= 20; i = i - 1)
                imm_J[i] = inst[31];
            imm_J[19:12] = inst[19:12];
            imm_J[11] = inst[20];
            imm_J[10:5] = inst[30:25];
            imm_J[4:1] = inst[24:21];
            imm_J[0] = 0;
        end
    endtask

    task decode;
        input [31:0] inst;

        wire [6:0] opcode;
        wire [4:0] rd;
        wire [2:0] funct3;
        wire [4:0] rs1;
        wire [4:0] rs2;
        wire [6:0] funct7;
        wire [4:0] shamt;
        wire [3:0] pred;
        wire [3:0] succ;
        wire [4:0] zimm;
        wire [11:0] csr;

        reg [31:0] imm;

        begin
            assign opcode = inst[6:0];
            assign rd = inst[11:7];
            assign funct3 = inst[14:12];
            assign rs1 = inst[19:15];
            assign rs2 = inst[24:20];
            assign funct7 = inst[31:25];
            assign shamt = inst[24:20];
            assign pred = inst[27:24];
            assign succ = inst[23:20];
            assign zimm = inst[19:15];
            assign csr = inst[31:20];

            case (opcode)
                `OPCODE_OP_IMM:
                    imm = imm_I (inst);
                `OPCODE_LUI, `OPCODE_AUIPC:
                    imm = imm_U (inst);
                `OPCODE_OP:
                `OPCODE_JAL:
                    imm = imm_J (inst);
                `OPCODE_JALR:
                    imm = imm_I (inst);
                `OPCODE_BRANCH:
                    imm = imm_B (inst);
                `OPCODE_LOAD:
                    imm = imm_I (inst);
                `OPCODE_STORE:
                    imm = imm_S (inst);
                `OPCODE_MISC_MEM:
                    imm = imm_I (inst);
            endcase

            case (opcode)
                `OPCODE_OP_IMM:
                    begin
                        case (func3):
                            `FUNCT3_ADDI:
                                
                    end
        end
    endtask



    always @ ( posedge inst_ready ) begin
        decode (inst);
    end

endmodule
