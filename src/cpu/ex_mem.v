module ex_mem (
        input wire clk,
        input wire rst,

        input wire [4:0] ex_wd,
        input wire ex_wreg,
        input wire [31:0] ex_wdata,

        output reg [4:0] mem_wd,
        output reg mem_wreg,
        output reg [31:0] mem_wdata,

        input wire [`MEM_OP_BUS] ex_mem_op,
        input wire [31:0] ex_mem_src,
        input wire [`MEM_SEL_BUS] ex_mem_sel,

        output reg [`MEM_OP_BUS] mem_op,
        output reg [31:0] mem_src,
        output reg [`MEM_SEL_BUS] mem_sel,

        input wire [5:0] stall
    );

    always @ (posedge clk) begin
        if (rst) begin
            mem_wd <= 0;
            mem_wreg <= 0;
            mem_wdata <= 0;
            mem_op <= 0;
            mem_src <= 0;
            mem_sel <= 0;
        end else if (stall[3] == 1 && stall[4] == 0) begin
            mem_wd <= 0;
            mem_wreg <= 0;
            mem_wdata <= 0;
            mem_op <= 0;
            mem_src <= 0;
            mem_sel <= 0;
        end else if (stall[3] == 0) begin
            mem_wd <= ex_wd;
            mem_wreg <= ex_wreg;
            mem_wdata <= ex_wdata;
            mem_op <= ex_mem_op;
            mem_src <= ex_mem_src;
            mem_sel <= ex_mem_sel;
        end
    end

endmodule // ex_mem
