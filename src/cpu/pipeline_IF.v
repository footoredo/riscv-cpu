module pipeline_IF (
        input CLK,
        input RST,

        input input_busy,
        input ID_busy,

        output fetch_inst_flag,
        output [31:0] inst_addr,

        input data_ready,
        input [31:0] data,

        input jump_flag,
        input [31:0] jump_PC,

        output reg [31:0] inst,
        output reg [31:0] inst_PC,
        output reg ready
    );

    reg [31:0] PC;
    wire [31:0] next_PC;
    assign next_PC = jump_flag ? jump_PC : PC + 4;
    assign fetch_inst_flag = !input_busy && !ID_busy;
    assign inst_addr = next_PC;

    always @(posedge CLK or posedge RST) begin
        if (RST) begin
            PC <= -32'd4;
            ready <= 0;
        end else begin
            if (input_busy) begin
                inst <= 0;
                inst_PC <= 0;
                ready <= 0;
            end
        end
    end

    always @(posedge data_ready) begin
        inst <= data;
        inst_PC <= next_PC;
        ready <= 1;
        PC <= next_PC;
    end
endmodule
