module cpu (
        input wire clk,
        input wire rst,

        input wire ram_inst_done_i,
        input wire [31:0] ram_inst_data_i,
        output wire [31:0] ram_inst_addr_o,
        output wire ram_inst_ce_o,

        // for debugging
        input wire req,
        input wire [4:0] req_addr,
        output wire [31:0] req_data,

        // ram
        input wire ram_mem_done_i,
        input wire [31:0] ram_mem_data_i,
        output wire [31:0] ram_mem_addr_o,
        output wire ram_mem_we_o,
        output wire [3:0] ram_mem_mask_o,
        output wire [31:0] ram_mem_data_o,
        output wire ram_mem_ce_o
    );

    wire [5:0] stall;
    wire if_stallreq;
    wire mem_stallreq;

    wire [31:0] pc;
    wire [31:0] id_pc_i;
    wire [31:0] id_inst_i;

    wire [`ALU_OP_BUS] id_aluop_o;
    wire [`ALU_SEL_BUS] id_alusel_o;
    wire [31:0] id_reg1_o;
    wire [31:0] id_reg2_o;
    wire id_wreg_o;
    wire [4:0] id_wd_o;
    wire [31:0] id_wdata_o;

    wire [`ALU_OP_BUS] ex_aluop_i;
    wire [`ALU_SEL_BUS] ex_alusel_i;
    wire [31:0] ex_reg1_i;
    wire [31:0] ex_reg2_i;
    wire ex_wreg_i;
    wire [4:0] ex_wd_i;
    wire [31:0] ex_wdata_i;

    wire ex_wreg_o;
    wire [4:0] ex_wd_o;
    wire [31:0] ex_wdata_o;

    wire mem_wreg_i;
    wire [4:0] mem_wd_i;
    wire [31:0] mem_wdata_i;

    wire mem_wreg_o;
    wire [4:0] mem_wd_o;
    wire [31:0] mem_wdata_o;

    wire wb_wreg_i;
    wire [4:0] wb_wd_i;
    wire [31:0] wb_wdata_i;

    wire reg1_read;
    wire reg2_read;
    wire [31:0] reg1_data;
    wire [31:0] reg2_data;
    wire [4:0] reg1_addr;
    wire [4:0] reg2_addr;

    // jump

    wire [31:0] invalid_mask;
    wire jump;
    wire [31:0] jump_addr;

    // mem

    wire [`MEM_OP_BUS] id_mem_op_o;
    wire [`MEM_OP_BUS] ex_mem_op_i;
    wire [`MEM_OP_BUS] ex_mem_op_o;
    wire [`MEM_OP_BUS] mem_op_i;
    wire [31:0] id_mem_src_o;
    wire [31:0] ex_mem_src_i;
    wire [31:0] ex_mem_src_o;
    wire [31:0] mem_src_i;
    wire [`MEM_SEL_BUS] id_mem_sel_o;
    wire [`MEM_SEL_BUS] ex_mem_sel_i;
    wire [`MEM_SEL_BUS] ex_mem_sel_o;
    wire [`MEM_SEL_BUS] mem_sel_i;
    wire [31:0] id_mem_offset_o;
    wire [31:0] ex_mem_offset_i;

    wire [31:0] if_inst;

    pc_reg pc_reg0 (
            .clk(clk), .rst(rst), .pc(pc),

            .jump(jump), .jump_addr(jump_addr), .stall(stall)
        );

    inf if0 (
            .rst(rst), .pc(pc), .stallreq(if_stallreq),
            .ram_addr_o(ram_inst_addr_o), .ram_data_i(ram_inst_data_i),
            .ram_ce_o(ram_inst_ce_o), .ram_done_i(ram_inst_done_i),
            .inst(if_inst)
        );

    assign rom_addr_o = pc;

    if_id if_id0 (
            .clk(clk), .rst(rst), .if_pc(pc),
            .if_inst(if_inst), .id_pc(id_pc_i),
            .kill_next(jump),
            .id_inst(id_inst_i), .stall(stall)
        );

    id id0 (
            .rst(rst), .pc_i(id_pc_i), .inst_i(id_inst_i),

            .reg1_data_i(reg1_data), .reg2_data_i(reg2_data),

            .reg1_read_o(reg1_read), .reg2_read_o(reg2_read),
            .reg1_addr_o(reg1_addr), .reg2_addr_o(reg2_addr),

            .aluop_o(id_aluop_o), .alusel_o(id_alusel_o),
            .reg1_o(id_reg1_o), .reg2_o(id_reg2_o),
            .wd_o(id_wd_o), .wreg_o(id_wreg_o),
            .wdata_o(id_wdata_o),

            .ex_wreg_i(ex_wreg_o), .ex_wd_i(ex_wd_o),
            .ex_wdata_i(ex_wdata_o),

            .mem_wreg_i(mem_wreg_o), .mem_wd_i(mem_wd_o),
            .mem_wdata_i(mem_wdata_o),

            .invalid_mask(invalid_mask), .jump(jump), .jump_addr(jump_addr),

            .mem_op_o(id_mem_op_o), .mem_src_o(id_mem_src_o),
            .mem_sel_o(id_mem_sel_o), .mem_offset_o(id_mem_offset_o)
        );

    regfile regfile1 (
            .clk(clk), .rst(rst),
            .we(wb_wreg_i), .waddr(wb_wd_i),
            .wdata(wb_wdata_i), .re1(reg1_read),
            .raddr1(reg1_addr), .rdata1(reg1_data),
            .re2(reg2_read), .raddr2(reg2_addr),
            .rdata2(reg2_data),
            .req(req), .req_addr(req_addr), .req_data(req_data)
        );

    id_ex id_ex0 (
            .clk(clk), .rst(rst),

            .id_aluop(id_aluop_o), .id_alusel(id_alusel_o),
            .id_reg1(id_reg1_o), .id_reg2(id_reg2_o),
            .id_wd(id_wd_o), .id_wreg(id_wreg_o),
            .id_wdata(id_wdata_o),

            .ex_aluop(ex_aluop_i), .ex_alusel(ex_alusel_i),
            .ex_reg1(ex_reg1_i), .ex_reg2(ex_reg2_i),
            .ex_wd(ex_wd_i), .ex_wreg(ex_wreg_i),
            .ex_wdata(ex_wdata_i),

            .id_mem_op(id_mem_op_o), .id_mem_src(id_mem_src_o),
            .id_mem_sel(id_mem_sel_o), .id_mem_offset(id_mem_offset_o),
            .ex_mem_op(ex_mem_op_i), .ex_mem_src(ex_mem_src_i),
            .ex_mem_sel(ex_mem_sel_i), .ex_mem_offset(ex_mem_offset_i),

            .stall(stall)
        );

    ex ex0 (
            .rst (rst),

            .aluop_i(ex_aluop_i), .alusel_i(ex_alusel_i),
            .reg1_i(ex_reg1_i), .reg2_i(ex_reg2_i),
            .wd_i(ex_wd_i), .wreg_i(ex_wreg_i),
            .wdata_i(ex_wdata_i),

            .mem_offset(ex_mem_offset_i),

            .wd_o(ex_wd_o), .wreg_o(ex_wreg_o),
            .wdata_o(ex_wdata_o)
        );

    ex_mem ex_mem0 (
            .clk(clk), .rst(rst),

            .ex_wd(ex_wd_o), .ex_wreg(ex_wreg_o),
            .ex_wdata(ex_wdata_o),

            .mem_wd(mem_wd_i), .mem_wreg(mem_wreg_i),
            .mem_wdata(mem_wdata_i),

            .ex_mem_op(ex_mem_op_i), .ex_mem_src(ex_mem_src_i), .ex_mem_sel(ex_mem_sel_i),
            .mem_op(mem_op_i), .mem_src(mem_src_i), .mem_sel(mem_sel_i),

            .stall(stall)
        );

    mem mem0 (
            .rst(rst),

            .wd_i(mem_wd_i), .wreg_i(mem_wreg_i),
            .wdata_i(mem_wdata_i),

            .wd_o(mem_wd_o), .wreg_o(mem_wreg_o),
            .wdata_o(mem_wdata_o),

            .op_i(mem_op_i), .src_i(mem_src_i), .sel_i(mem_sel_i),

            .ram_data_i(ram_mem_data_i),
            .ram_addr_o(ram_mem_addr_o),
            .ram_we_o(ram_mem_we_o),
            .ram_mask_o(ram_mem_mask_o),
            .ram_data_o(ram_mem_data_o),
            .ram_ce_o(ram_mem_ce_o),
            .ram_done(ram_mem_done_i),

            .stallreq(mem_stallreq)
        );

    mem_wb mem_wb0 (
            .clk(clk), .rst(rst),

            .mem_wd(mem_wd_o), .mem_wreg(mem_wreg_o),
            .mem_wdata(mem_wdata_o),

            .wb_wd(wb_wd_i), .wb_wreg(wb_wreg_i),
            .wb_wdata(wb_wdata_i),

            .stall(stall)
        );

    ctrl ctrl0 (
            .rst(rst),
            .stallreq_from_if(if_stallreq),
            .stallreq_from_mem(mem_stallreq),
            .stall(stall)
        );
/*
    ram_processor ram_processor0 (
            .clk(clk), .rst(rst),

            .inst_ce(ram_inst_ce_o), .inst_addr(ram_inst_addr_o),
            .inst_data(ram_inst_data_i), .inst_done(ram_inst_done_i),

            .mem_ce(ram_mem_ce_o), .mem_addr(ram_mem_addr_o),
            .mem_we(ram_mem_we_o), .mem_mask(ram_mem_mask_o),
            .mem_data_o(ram_mem_data_i), .mem_data_i(ram_mem_data_o),
            .mem_done(ram_mem_done_i)
        );*/

endmodule // cpu
