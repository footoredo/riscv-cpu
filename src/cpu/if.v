module inf (
        input wire rst,
        input wire [31:0] pc,
        input wire [31:0] ram_data_i,
        input wire ram_done_i,

        output reg ram_ce_o,
        output reg [31:0] ram_addr_o,
        output reg [31:0] inst,
        output reg stallreq
    );

    always @ ( * ) begin
        if (rst) begin
            ram_ce_o <= 0;
            ram_addr_o <= 0;
            stallreq <= 0;
        end else begin
            ram_ce_o <= 1;
            // $displayh (ram_data_i);
            // inst <= {ram_data_i[7:0], ram_data_i[15:8], ram_data_i[23:16], ram_data_i[31:24]};
            ram_addr_o <= pc;
            stallreq <= 1;
        end
    end

    always @ ( * ) begin
        if (ram_done_i) begin
            inst <= {ram_data_i[7:0], ram_data_i[15:8], ram_data_i[23:16], ram_data_i[31:24]};
            ram_ce_o <= 0;
            stallreq <= 0;
        end
    end

endmodule // if
