module if_id (
        input wire clk,
        input wire rst,

        input wire [31:0] if_pc,
        input wire [31:0] if_inst,

        output reg [31:0] id_pc,
        output reg [31:0] id_inst,

        input wire kill_next,

        input wire [5:0] stall
    );

    reg kill_next_bak;

    always @ (posedge clk) begin
        if (rst) begin
            id_pc <= 0;
            id_inst <= 0;
            kill_next_bak <= 0;
        end else if (stall[1] == 1) begin
            if (kill_next)
                kill_next_bak <= 1;
            if (stall[2] == 0) begin
                id_pc <= 0;
                id_inst <= 0;
            end
        end else if (stall[1] == 0) begin
            id_pc <= if_pc;
            if (kill_next || kill_next_bak)
                id_inst <= `NOP_INST;
            else
                id_inst <= if_inst;
            kill_next_bak <= 0;
        end
    end

endmodule
