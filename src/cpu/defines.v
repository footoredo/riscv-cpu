`define OPCODE_LOAD  7'b0000011
`define OPCODE_LOAD_FP  7'b0000111
`define OPCODE_MISC_MEM  7'b0001111
`define OPCODE_OP_IMM  7'b0010011
`define OPCODE_AUIPC  7'b0010111
`define OPCODE_OP_IMM_32  7'b0011011
`define OPCODE_STORE  7'b0100011
`define OPCODE_STORE_FP  7'b0100111
`define OPCODE_AMO  7'b0101111
`define OPCODE_OP  7'b0110011
`define OPCODE_LUI  7'b0110111
`define OPCODE_OP_32  7'b0111011
`define OPCODE_MADD  7'b1000011
`define OPCODE_MSUB  7'b1000111
`define OPCODE_NMSUB  7'b1001011
`define OPCODE_NMADD  7'b1001111
`define OPCODE_OP_FP  7'b1010011
`define OPCODE_BRANCH  7'b1100011
`define OPCODE_JALR  7'b1100111
`define OPCODE_JAL  7'b1101111
`define OPCODE_SYSTEM  7'b1110011

`define FUNCT3_JALR  3'b000
`define FUNCT3_BEQ  3'b000
`define FUNCT3_BNE  3'b001
`define FUNCT3_BLT  3'b100
`define FUNCT3_BGE  3'b101
`define FUNCT3_BLTU  3'b110
`define FUNCT3_BGEU  3'b111
`define FUNCT3_LB  3'b000
`define FUNCT3_LH  3'b001
`define FUNCT3_LW  3'b010
`define FUNCT3_LBU  3'b100
`define FUNCT3_LHU  3'b101
`define FUNCT3_SB  3'b000
`define FUNCT3_SH  3'b001
`define FUNCT3_SW  3'b010
`define FUNCT3_ADDI  3'b000
`define FUNCT3_SLTI  3'b010
`define FUNCT3_SLTIU  3'b011
`define FUNCT3_XORI  3'b100
`define FUNCT3_ORI  3'b110
`define FUNCT3_ANDI  3'b111
`define FUNCT3_SLLI  3'b001
`define FUNCT3_SRLI  3'b101
`define FUNCT3_SRAI  3'b101
`define FUNCT3_ADD  3'b000
`define FUNCT3_SUB  3'b000
`define FUNCT3_SLL  3'b001
`define FUNCT3_SLT  3'b010
`define FUNCT3_SLTU  3'b011
`define FUNCT3_XOR  3'b100
`define FUNCT3_SRL  3'b101
`define FUNCT3_SRA  3'b101
`define FUNCT3_OR  3'b110
`define FUNCT3_AND  3'b111
`define FUNCT3_FENCE  3'b000
`define FUNCT3_FENCE_I  3'b001
`define FUNCT3_ECALL  3'b000
`define FUNCT3_EBREAK  3'b000
`define FUNCT3_CSRRW  3'b001
`define FUNCT3_CSRRS  3'b010
`define FUNCT3_CSRRC  3'b011
`define FUNCT3_CSRRWI  3'b101
`define FUNCT3_CSRRSI  3'b110
`define FUNCT3_CSRRCI  3'b111

`define FUNCT7_ADD 7'b0000000
`define FUNCT7_SUB 7'b0100000

`define FUNCT7_SRL 7'b0000000
`define FUNCT7_SRA 7'b0100000

`define ALU_OP_BUS  4:0
`define ALU_OP_NOP  0
`define ALU_OP_OR  1
`define ALU_OP_AND 2
`define ALU_OP_XOR 3
`define ALU_OP_ADD 4
`define ALU_OP_SLT 5
`define ALU_OP_SLL 6
`define ALU_OP_SRL 7
`define ALU_OP_SRA 8
`define ALU_OP_SLTU 9
`define ALU_OP_SUB 10
`define ALU_OP_ADD_MEM 11

`define ALU_SEL_BUS  2:0
`define ALU_SEL_NOP  0
`define ALU_SEL_LOGIC  1
`define ALU_SEL_ARITH  2

`define INST_MEM_NUM  1024
`define INST_MEM_NUM_LOG2  10

`define INVALID 32'hffffffff
`define VALID 32'h00000000

`define NOP_INST 32'h00000000
`define OFFSET_MASK 32'hfffffffe

`define MEM_SEL_BUS 2:0
`define MEM_SEL_NOP 0
`define MEM_SEL_LOAD 1
`define MEM_SEL_STORE 2

`define MEM_OP_BUS 4:0
`define MEM_OP_NOP 0
`define MEM_OP_LW 1
`define MEM_OP_LH 2
`define MEM_OP_LHU 3
`define MEM_OP_LB 4
`define MEM_OP_LBU 5
`define MEM_OP_SW 6
`define MEM_OP_SH 7
`define MEM_OP_SB 8

`define FINISH_INST 32'hffffffff

`define MEM_SIZE 1048576
`define MEM_SIZE_LOG 20

`define STATUS_WAITING 0
`define STATUS_FINISHED 1
`define STATUS_BUSY 2
`define STATUS_BUSY2 3

`define TERM_ADDR 32'h00000108
`define TERM_MASK 8
`define TERM_DATA 32'hffffffff
`define OUTPUT_ADDR 32'h00000104
`define INPUT_ADDR 32'h00000100
