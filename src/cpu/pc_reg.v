module pc_reg (
        input wire clk,
        input wire rst,
        output reg[31:0] pc,
        output reg ce,

        input wire jump,
        input wire [31:0] jump_addr,

        input wire [5:0] stall
    );

    reg jump_bak;
    reg [31:0] jump_addr_bak;

    always @ (posedge clk) begin
        if (rst) begin
            ce <= 0;
        end else begin
            ce <= 1;
        end
    end

    always @ (posedge clk) begin
        if (ce == 0) begin
            pc <= 0;
            end else begin
            if (stall[0] == 0) begin
                if (jump) begin
                    pc <= jump_addr;
                end else if (jump_bak) begin
                    pc <= jump_addr_bak;
                end else begin
                    pc <= pc + 4;
                end
                jump_bak <= 0;
                jump_addr_bak <= 0;
            end else if (jump) begin
                jump_bak <= jump;
                jump_addr_bak <= jump_addr;
            end
        end
    end

endmodule
