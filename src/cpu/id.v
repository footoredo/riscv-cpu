module id (
        input wire rst,
        input wire [31:0] pc_i,
        input wire [31:0] inst_i,

        input wire [31:0] reg1_data_i,
        input wire [31:0] reg2_data_i,

        output reg reg1_read_o,
        output reg reg2_read_o,
        output reg [4:0] reg1_addr_o,
        output reg [4:0] reg2_addr_o,

        output reg [`ALU_SEL_BUS] alusel_o,
        output reg [`ALU_OP_BUS] aluop_o,
        output reg [31:0] reg1_o,
        output reg [31:0] reg2_o,
        output reg [31:0] wdata_o,
        output reg [4:0] wd_o,
        output reg wreg_o,

        // forwarding
        input wire ex_wreg_i,
        input wire [4:0] ex_wd_i,
        input wire [31:0] ex_wdata_i,

        input wire mem_wreg_i,
        input wire [4:0] mem_wd_i,
        input wire [31:0] mem_wdata_i,

        // invalid for controlflow
        input wire [31:0] invalid_mask,
        output reg jump,
        output reg [31:0] jump_addr,

        // mem op
        output reg [`MEM_OP_BUS] mem_op_o,
        output wire [31:0] mem_src_o,
        output reg [`MEM_SEL_BUS] mem_sel_o,
        output reg [31:0] mem_offset_o
    );

    assign mem_src_o = reg2_o;

/*    function [31:0] imm_I;
        input [31:0] inst;
        begin
            imm_I <= {{21{inst[31]}}, inst[30:25], inst[24:21], inst[20]};
        end
    endfunction

    function [31:0] imm_S;
        input [31:0] inst;
        begin
            imm_S <= {{21{inst[31]}}, inst[30:25], inst[11:8], inst[7]};
        end
    endfunction

    function [31:0] imm_B;
        input [31:0] inst;
        begin
            imm_B <= {{20{inst[31]}}, inst[7], inst[30:25], inst[11:8], 0};
        end
    endfunction

    function [31:0] imm_U;
        input [31:0] inst;
        begin
            imm_U = {inst[31], inst[30:20], inst[19:12], 12'b0};
        end
    endfunction

    function [31:0] imm_J;
        input [31:0] inst;
        begin
            imm_J = {{12{inst[31]}}, inst[19:12], inst[20], inst[30:25], inst[24:21], 0};
        end
    endfunction*/

    wire [31:0] next_pc;
    assign next_pc = pc_i + 4;

    wire [6:0] opcode;
    wire [4:0] rd;
    wire [2:0] funct3;
    wire [4:0] rs1;
    wire [4:0] rs2;
    wire [6:0] funct7;
    wire [4:0] shamt;
    wire [3:0] pred;
    wire [3:0] succ;
    wire [4:0] zimm;
    wire [11:0] csr;

    wire [31:0] inst_im;
    wire [31:0] inst;
    // assign finished = inst_im == `FINISH_INST;
    assign inst = inst_i;

    reg [31:0] imm;

    //reg instvalid;

    assign opcode = inst[6:0];
    assign rd = inst[11:7];
    assign funct3 = inst[14:12];
    assign rs1 = inst[19:15];
    assign rs2 = inst[24:20];
    assign funct7 = inst[31:25];
    assign shamt = inst[24:20];
    assign pred = inst[27:24];
    assign succ = inst[23:20];
    assign zimm = inst[19:15];
    assign csr = inst[31:20];

    reg reg1_read_imm;
    reg reg2_read_imm;
    reg wdata_read_imm;
    reg is_jalr;

    always @ (*) begin
        if (rst) begin
            aluop_o <= `ALU_OP_NOP;
            alusel_o <= `ALU_SEL_NOP;
            wd_o <= 0;
            wreg_o <= 0;
            //instvalid <= 0;
            reg1_read_o <= 0;
            reg2_read_o <= 0;
            reg1_addr_o <= 0;
            reg2_addr_o <= 0;
            reg1_read_imm <= 0;
            reg2_read_imm <= 0;
            wdata_read_imm <= 0;
            jump <= 0;
            jump_addr <= 0;
            is_jalr <= 0;
            mem_op_o <= `MEM_OP_NOP;
            mem_sel_o <= `MEM_SEL_NOP;
        end else begin
            aluop_o <= `ALU_OP_NOP;
            alusel_o <= `ALU_SEL_NOP;
            wd_o <= rd;
            wreg_o <= 0;
            //instvalid <= 0;
            reg1_read_o <= 0;
            reg2_read_o <= 0;
            reg1_addr_o <= rs1;
            reg2_addr_o <= rs2;
            reg1_read_imm <= 0;
            reg2_read_imm <= 0;
            wdata_read_imm <= 0;
            jump <= 0;
            is_jalr <= 0;
            mem_op_o <= `MEM_OP_NOP;
            mem_sel_o <= `MEM_SEL_NOP;

//            $displayh(inst);

            case (opcode)
                `OPCODE_OP_IMM:
                    begin
                        wreg_o <= 1;
                        reg1_read_o <= 1;
                        case (funct3)
                            `FUNCT3_ORI: begin
                                reg2_read_imm <= 1;
                                aluop_o <= `ALU_OP_OR;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_ANDI: begin
                                reg2_read_imm <= 1;
                                aluop_o <= `ALU_OP_AND;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_XORI: begin
                                reg2_read_imm <= 1;
                                aluop_o <= `ALU_OP_XOR;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_ADDI: begin
                                reg2_read_imm <= 1;
                                aluop_o <= `ALU_OP_ADD;
                                alusel_o <= `ALU_SEL_ARITH;
                            end
                            `FUNCT3_SLTI: begin
                                reg2_read_imm <= 1;
                                aluop_o <= `ALU_OP_SLT;
                                alusel_o <= `ALU_SEL_ARITH;
                            end
                            `FUNCT3_SLTIU: begin
                                reg2_read_imm <= 1;
                                aluop_o <= `ALU_OP_SLTU;
                                alusel_o <= `ALU_SEL_ARITH;
                            end
                            `FUNCT3_SLLI: begin
                                reg2_read_imm <= 1;
                                // reg2_o <= shamt;
                                imm <= shamt;
                                aluop_o <= `ALU_OP_SLL;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_SRLI: begin
                                reg2_read_imm <= 1;
                                case (funct7)
                                    `FUNCT7_SRL: begin
                                        aluop_o <= `ALU_OP_SRL;
                                        alusel_o <= `ALU_SEL_LOGIC;
                                    end
                                    `FUNCT7_SRA: begin
                                        aluop_o <= `ALU_OP_SRA;
                                        alusel_o <= `ALU_SEL_ARITH;
                                    end
                                endcase
                            end
                        endcase
                    end
                `OPCODE_LUI:
                    begin
                        wreg_o <= 1;
                        wdata_read_imm <= 1;
                    end
                `OPCODE_AUIPC:
                    begin
                        reg1_o <= pc_i;
                        reg2_read_imm <= 1;
                        wreg_o <= 1;
                        aluop_o <= `ALU_OP_ADD;
                        alusel_o <= `ALU_SEL_ARITH;
                    end
                `OPCODE_OP:
                    begin
                        wreg_o <= 1;
                        reg1_read_o <= 1;
                        reg2_read_o <= 1;
                        case (funct3)
                            `FUNCT3_ADD: begin
                                case (funct7)
                                    `FUNCT7_ADD: begin
                                        aluop_o <= `ALU_OP_ADD;
                                        alusel_o <= `ALU_SEL_ARITH;
                                    end
                                    `FUNCT7_SUB: begin
                                        aluop_o <= `ALU_OP_SUB;
                                        alusel_o <= `ALU_SEL_ARITH;
                                    end
                                endcase
                            end
                            `FUNCT3_SLT: begin
                                aluop_o <= `ALU_OP_SLT;
                                alusel_o <= `ALU_SEL_ARITH;
                            end
                            `FUNCT3_SLTU: begin
                                aluop_o <= `ALU_OP_SLTU;
                                alusel_o <= `ALU_SEL_ARITH;
                            end
                            `FUNCT3_AND: begin
                                aluop_o <= `ALU_OP_AND;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_OR: begin
                                aluop_o <= `ALU_OP_OR;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_XOR: begin
                                aluop_o <= `ALU_OP_XOR;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_SLL: begin
                                aluop_o <= `ALU_OP_SLL;
                                alusel_o <= `ALU_SEL_LOGIC;
                            end
                            `FUNCT3_SRL: begin
                                case (funct7)
                                    `FUNCT7_SRL: begin
                                        aluop_o <= `ALU_OP_SRL;
                                        alusel_o <= `ALU_SEL_LOGIC;
                                    end
                                    `FUNCT7_SRA: begin
                                        aluop_o <= `ALU_OP_SRA;
                                        alusel_o <= `ALU_SEL_ARITH;
                                    end
                                endcase
                            end
                        endcase
                    end
                `OPCODE_JAL:
                    begin
                        wreg_o <= 1;
                        wdata_o <= next_pc;
                        jump <= 1;
                    end
                `OPCODE_JALR:
                    begin
                        wreg_o <= 1;
                        wdata_o <= next_pc;
                        jump <= 1;
                        reg1_read_o <= 1;
                        is_jalr <= 1;
                    end
                `OPCODE_BRANCH:
                    begin
                        reg1_read_o <= 1;
                        reg2_read_o <= 1;
                    end
                `OPCODE_LOAD:
                    begin
                        wreg_o <= 1;
                        reg1_read_o <= 1;
                        aluop_o <= `ALU_OP_ADD_MEM;
                        alusel_o <= `ALU_SEL_ARITH;
                        mem_sel_o <= `MEM_SEL_LOAD;
                        case (funct3)
                            `FUNCT3_LW: mem_op_o <= `MEM_OP_LW;
                            `FUNCT3_LH: mem_op_o <= `MEM_OP_LH;
                            `FUNCT3_LHU: mem_op_o <= `MEM_OP_LHU;
                            `FUNCT3_LB: mem_op_o <= `MEM_OP_LB;
                            `FUNCT3_LBU: mem_op_o <= `MEM_OP_LBU;
                        endcase
                    end
                `OPCODE_STORE:
                    begin
                        reg1_read_o <= 1;
                        reg2_read_o <= 1;
                        aluop_o <= `ALU_OP_ADD_MEM;
                        alusel_o <= `ALU_SEL_ARITH;
                        mem_sel_o <= `MEM_SEL_STORE;
                        case (funct3)
                            `FUNCT3_SW: mem_op_o <= `MEM_OP_SW;
                            `FUNCT3_SH: mem_op_o <= `MEM_OP_SH;
                            `FUNCT3_SB: mem_op_o <= `MEM_OP_SB;
                        endcase
                    end
                0: begin
                end
                default: begin
                    $display ("What the fuck is %b ??", opcode);
                end
            endcase
        end
    end

    always @ ( * ) begin
        if (opcode == `OPCODE_BRANCH) begin
            case (funct3)
                `FUNCT3_BEQ: begin
                    jump <= reg1_o == reg2_o;
                end
                `FUNCT3_BNE: begin
                    jump <= reg1_o != reg2_o;
                end
                `FUNCT3_BLT: begin
                    jump <= $signed(reg1_o) < $signed(reg2_o);
                end
                `FUNCT3_BLTU: begin
                    jump <= reg1_o < reg2_o;
                end
                `FUNCT3_BGE: begin
                    jump <= $signed(reg1_o) > $signed(reg2_o);
                end
                `FUNCT3_BGEU: begin
                    jump <= reg1_o > reg2_o;
                end
            endcase
        end
    end

    always @ ( * ) begin
        if (rst) begin
            imm <= 0;
        end else begin
            case (opcode)
                `OPCODE_OP_IMM:
                    imm <= {{21{inst[31]}}, inst[30:25], inst[24:21], inst[20]};
                `OPCODE_LUI, `OPCODE_AUIPC:
                    imm <= {inst[31], inst[30:20], inst[19:12], 12'b0};
                `OPCODE_JAL:
                    imm <= {{12{inst[31]}}, inst[19:12], inst[20], inst[30:25], inst[24:21], 1'b0};
                `OPCODE_JALR:
                    imm <= {{21{inst[31]}}, inst[30:25], inst[24:21], inst[20]};
                `OPCODE_BRANCH:
                    imm <= {{20{inst[31]}}, inst[7], inst[30:25], inst[11:8], 1'b0};
                `OPCODE_LOAD:
                    imm <= {{21{inst[31]}}, inst[30:25], inst[24:21], inst[20]};
                `OPCODE_STORE:
                    imm <= {{21{inst[31]}}, inst[30:25], inst[11:8], inst[7]};
                `OPCODE_MISC_MEM:
                    imm <= {{21{inst[31]}}, inst[30:25], inst[24:21], inst[20]};
            endcase
        end
    end

    always @ ( * ) begin
        mem_offset_o <= imm;
    end

    always @ ( * ) begin
        if (wdata_read_imm) begin
            wdata_o <= imm;
        end
    end

    always @ ( * ) begin
        if (reg1_read_imm) begin
            reg1_o <= imm;
        end
    end

    always @ ( * ) begin
        if (reg2_read_imm) begin
            reg2_o <= imm;
        end
    end

    always @ ( * ) begin
        if (rst) begin
            reg1_o <= 0;
        end else if (reg1_read_o == 1) begin
            // $display("read_o");
            if (rs1 == 0) begin
                reg1_o <= 0;
            end else if (reg1_read_o && ex_wreg_i && ex_wd_i == rs1) begin
                reg1_o <= ex_wdata_i;
            end else if (reg1_read_o && mem_wreg_i && mem_wd_i == rs1) begin
                reg1_o <= mem_wdata_i;
            end else begin
                reg1_o <= reg1_data_i;
            end
        end
    end

    always @ ( * ) begin
        if (rst) begin
            reg2_o <= 0;
        end else if (reg2_read_o == 1) begin
            // $display("read_o");
            if (rs2 == 0) begin
                reg2_o <= 0;
            end else if (reg2_read_o && ex_wreg_i && ex_wd_i == rs2) begin
                reg2_o <= ex_wdata_i;
            end else if (reg2_read_o && mem_wreg_i && mem_wd_i == rs2) begin
                reg2_o <= mem_wdata_i;
            end else begin
                reg2_o <= reg2_data_i;
            end
        end
    end

    always @ ( * ) begin
        if (is_jalr) begin
            jump_addr <= (imm + reg1_o) & `OFFSET_MASK;
        end else begin
            jump_addr <= pc_i + imm;
        end
    end

endmodule
