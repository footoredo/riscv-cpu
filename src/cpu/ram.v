`timescale 1ns / 1ps

module ram_processor (
        input wire clk,
        input wire rst,

        input wire inst_ce,
        input wire [31:0] inst_addr,
        output reg [31:0] inst_data,
        output reg inst_done,

        input wire mem_ce,
        input wire mem_we,
        input wire [31:0] mem_addr,
        input wire [3:0] mem_mask,
        input wire [31:0] mem_data_i,
        output reg [31:0] mem_data_o,
        output reg mem_done,
        output reg task_sent,

        output wire finished
    );

    reg ce;
    reg we;
    reg [31:0] addr;
    reg [3:0] mask;
    reg [31:0] data_i;
    wire [31:0] data_o;
    wire busy;

    // reg task_sent;
    reg job; // 0 for mem, 1 for inst
    reg [3:0] status;

    ram ram0 (
            .clk(clk), .rst(rst),
            .ce(ce), .we(we),
            .addr(addr), .mask(mask),
            .data_i(data_i), .data_o(data_o),
            .busy(busy), .finished(finished)
        );

    always @ (posedge clk) begin
        if (rst) begin
            inst_data <= 0;
            inst_done <= 0;
            mem_data_o <= 0;
            mem_done <= 0;
            ce <= 0;
            we <= 0;
            addr <= 0;
            mask <= 0;
            data_i <= 0;
            task_sent <= 0;
            status <= `STATUS_WAITING;
        end else if (status == `STATUS_WAITING && !busy) begin
            if (mem_ce) begin
                task_sent <= 1;
                ce <= mem_ce;
                we <= mem_we;
                addr <= mem_addr;
                data_i <= mem_data_i;
                mask <= mem_mask;
                job <= 0;
                mem_done <= 0;
                status <= `STATUS_BUSY;
            end else if (inst_ce) begin
                task_sent <= 1;
                ce <= inst_ce;
                we <= 0;
                addr <= inst_addr;
                data_i <= 0;
                mask <= 4'b1111;
                job <= 1;
                inst_done <= 0;
                status <= `STATUS_BUSY;
            end else begin
                inst_data <= 0;
                inst_done <= 0;
                mem_data_o <= 0;
                mem_done <= 0;
                ce <= 0;
                we <= 0;
                addr <= 0;
                mask <= 0;
                data_i <= 0;
                task_sent <= 0;
            end
        end
    end

    always @ ( posedge clk ) begin
        if (status == `STATUS_BUSY) begin
            status <= `STATUS_BUSY2;
        end
    end

    always @ (posedge clk) begin
        if (status == `STATUS_BUSY2 && !busy) begin
            ce <= 0;
            task_sent <= 0;
            status <= `STATUS_WAITING;
            if (job == 0) begin
                mem_data_o <= data_o;
                mem_done <= 1;
            end else if (job == 1) begin
                inst_data <= data_o;
                inst_done <= 1;
            end
        end
    end

endmodule

module ram (
        input wire clk,
        input wire rst,
        input wire ce,
        input wire we,
        input wire [31:0] addr,
        input wire [3:0] mask,
        input wire [31:0] data_i,
        output reg [31:0] data_o,
        output reg busy,
        output reg finished
    );

    reg [31:0] data_mem[0:`MEM_SIZE - 1];
    reg [2:0] status;

    initial begin
        $readmemh ( "data/memory.data", data_mem );
        status <= `STATUS_WAITING;
        finished <= 0;
    end

    wire [`MEM_SIZE_LOG - 1: 0] addr_p;
    assign addr_p = addr[`MEM_SIZE_LOG + 1: 2];

    integer i;

    integer data_file;
    integer scan_file;

    initial begin
        data_file = $fopen("data/in.data", "r");
    end

    always @ (posedge clk) begin
        if (rst) begin
            busy <= 0;
        end else if (status == `STATUS_WAITING) begin
            if (ce) begin
                busy <= 1;
                status <= `STATUS_BUSY;
                if (we) begin
                    // $write ("\nWriting %h (%h) %h, mask %b\n", addr, addr_p, data_i, mask);
                    // if (addr_p == 12'hfff) $display("fuck");
                    if (addr == `OUTPUT_ADDR) $write ("%c", data_i[31:24]);
                    if (mask[3]) data_mem[addr_p][31:24] <= data_i[31:24];
                    if (mask[2]) data_mem[addr_p][23:16] <= data_i[23:16];
                    if (mask[1]) data_mem[addr_p][15:8] <= data_i[15:8];
                    if (mask[0]) data_mem[addr_p][7:0] <= data_i[7:0];
                end else begin
                // $write ("\nReading %h %h %b\n", addr, data_mem[addr_p], mask);
                    if (addr == `INPUT_ADDR) begin
                        scan_file = $fscanf(data_file, "%d", data_o);
                    end else begin
                        data_o = data_mem[addr_p];
                    end
                end
                #5 busy <= 0;
            end
        end else if (status == `STATUS_BUSY) begin
            status <= `STATUS_FINISHED;
            if (we == 1 && addr == `TERM_ADDR && mask == `TERM_MASK && data_i == `TERM_DATA) begin
                $finish;
            end
        end else if (status == `STATUS_FINISHED) begin
            status <= `STATUS_WAITING;
        end
    end
endmodule
