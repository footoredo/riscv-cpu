module id_ex (
        input wire clk,
        input wire rst,

        input wire [`ALU_OP_BUS] id_aluop,
        input wire [`ALU_SEL_BUS] id_alusel,
        input wire [31:0] id_reg1,
        input wire [31:0] id_reg2,
        input wire [4:0] id_wd,
        input wire id_wreg,
        input wire [31:0] id_wdata,

        output reg [`ALU_OP_BUS] ex_aluop,
        output reg [`ALU_SEL_BUS] ex_alusel,
        output reg [31:0] ex_reg1,
        output reg [31:0] ex_reg2,
        output reg [4:0] ex_wd,
        output reg ex_wreg,
        output reg [31:0] ex_wdata,

        input wire [`MEM_OP_BUS] id_mem_op,
        input wire [31:0] id_mem_src,
        input wire [`MEM_SEL_BUS] id_mem_sel,
        input wire [31:0] id_mem_offset,

        output reg [`MEM_OP_BUS] ex_mem_op,
        output reg [31:0] ex_mem_src,
        output reg [`MEM_SEL_BUS] ex_mem_sel,
        output reg [31:0] ex_mem_offset,

        input wire [5:0] stall
    );

    always @ ( posedge clk ) begin
        if (rst) begin
            ex_aluop <= 0;
            ex_alusel <= 0;
            ex_reg1 <= 0;
            ex_reg2 <= 0;
            ex_wd <= 0;
            ex_wreg <= 0;
            ex_wdata <= 0;
            ex_mem_op <= 0;
            ex_mem_src <= 0;
            ex_mem_sel <= 0;
            ex_mem_offset <= 0;
        end else if (stall[2] == 1 && stall[3] == 0) begin
            ex_aluop <= 0;
            ex_alusel <= 0;
            ex_reg1 <= 0;
            ex_reg2 <= 0;
            ex_wd <= 0;
            ex_wreg <= 0;
            ex_wdata <= 0;
            ex_mem_op <= 0;
            ex_mem_src <= 0;
            ex_mem_sel <= 0;
            ex_mem_offset <= 0;
        end else if (stall[2] == 0) begin
            ex_aluop <= id_aluop;
            ex_alusel <= id_alusel;
            ex_reg1 <= id_reg1;
            ex_reg2 <= id_reg2;
            ex_wd <= id_wd;
            ex_wreg <= id_wreg;
            ex_wdata <= id_wdata;
            ex_mem_op <= id_mem_op;
            ex_mem_src <= id_mem_src;
            ex_mem_sel <= id_mem_sel;
            ex_mem_offset <= id_mem_offset;
        end
    end

endmodule
