module regfile (
        input wire clk,
        input wire rst,

        input wire we,
        input wire [4:0] waddr,
        input wire [31:0] wdata,

        input wire re1,
        input wire [4:0] raddr1,
        output reg [31:0] rdata1,

        input wire re2,
        input wire [4:0] raddr2,
        output reg [31:0] rdata2,

        input wire req,
        input wire [4:0] req_addr,
        output reg [31:0] req_data
    );

    reg [31:0] regs [31:0];

    always @ (posedge clk) begin
        if (!rst) begin
            if (we && waddr != 0) begin
                regs[waddr] <= wdata;
                // if (waddr == 2) $displayh (wdata);
            end
        end
    end

    always @ (*) begin
        if (rst || raddr1 == 0 || !re1)
            rdata1 <= 0;
        else if (we && waddr == raddr1)
            rdata1 <= wdata;
        else
            rdata1 <= regs[raddr1];
    end

    always @ (*) begin
        if (rst || raddr2 == 0 || !re2)
            rdata2 <= 0;
        else if (we && waddr == raddr2)
            rdata2 <= wdata;
        else
            rdata2 <= regs[raddr2];
    end

    always @ ( * ) begin
        if (rst || !req || req_addr == 0)
            req_data <= 0;
        else if (we && waddr == req_addr)
            req_data <= wdata;
        else
            req_data <= regs[req_addr];
    end

endmodule
