#include <bits/stdc++.h>

int x[32];

char _tohex(unsigned int x) {
    if (x < 10) return x + '0';
    else return x - 10 + 'a';
}

std::string tohex (unsigned int x, int len, std::string prefix = "") {
    std::string ans = prefix;
    for (int i = 0; i < len; ++ i) {
        ans += _tohex((x & (0xf << (4 * (len - 1 - i)))) >> (4 * (len - 1 - i)));
    }
    return ans;
}

int main () {
    srand(time (0));
    std::ofstream ofile("test.s");
    std::ofstream std_data("std.data");

    x[0] = 0;
    for (int i = 1; i < 32; ++ i) {
        for (int j = 0; j < 5; ++ j) {
            int other = rand () % i;
            int num = rand () & 0xff;
            int op = rand () % 3;
            if (op == 0) {
                ofile << "ori";
                x[i] = x[other] | num;
            }
            else if (op == 1) {
                ofile << "andi";
                x[i] = x[other] & num;
            }
            else {
                ofile << "xori";
                x[i] = x[other] ^ num;
            }
            ofile << " x" << i << ",x" << other << ',' << tohex(num, 2, "0x") << std::endl;
        }
    }

    for (int i = 0; i < 32; ++ i) {
        std_data << tohex (i, 8) << ' ' << tohex(x[i], 8) << std::endl;
    }
}
