#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
const int NSIZE = 8;

int main(int argc,char *argv[])
{
    if (argc == 1||argc > 2)
    {
        cerr << "Please input an binary file." << endl;
        return 0;
    }
    ifstream ifile(argv[1],ios::in|ios::binary);
    if (!ifile)
    {
        cerr << "Cannot open file." << endl;
        return 0;
    }
    /*int head = ifile.tellg(),tail = (ifile.seekg(0,ios::end)).tellg();
    ifile.seekg(0,ios::beg);
    int N = (tail-head)/4;
    //cerr << head << ' ' << tail << endl;
    while (N--)
    {
        ll num = 0; int now = 0;
        for (int k = 0;k < 4;++k)
        {
            unsigned char c; ifile >> c;
            cerr << k << ' ' << (int)c << '\n';
            for (int i = 0;i < NSIZE;++i)
                num |= (!!(c&((unsigned char)(1)<<i)))<<(now++);
        }
        cout.width(8); cout.fill('0');
        cout << hex << num << endl;
    }*/

    vector<unsigned char> fileData((istreambuf_iterator<char>(ifile)), istreambuf_iterator<char>());

    int inst_n = fileData.size() / 4;
    for (int i = 0; i < inst_n; ++ i) {
        unsigned int num = 0;
        for (int k = 0;k < 4;++k)
        {
            unsigned char c = fileData[i * 4 + 3 - k];
            num ^= (unsigned int)c << (k * 8);
        }
        cout.width(8); cout.fill('0');
        cout << hex << num << endl;
    }

    // cout << "ffffffff" << std::endl;
    return 0;
}
