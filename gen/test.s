	.file	"test.c"
	.option nopic
	.comm	tmp,40,4
	.comm	top,40,4
	.comm	len,4,4
	.comm	f,40,4
	.comm	x,4,4
	.comm	i,4,4
	.text
	.align	1
	.globl	main
	.type	main, @function
main:
	addi	sp,sp,-16
	sw	s0,12(sp)
	addi	s0,sp,16
	lui	a5,%hi(f)
	addi	a5,a5,%lo(f)
	li	a4,1
	sw	a4,4(a5)
	lui	a5,%hi(i)
	li	a4,2
	sw	a4,%lo(i)(a5)
	j	.L2
.L3:
	lui	a5,%hi(i)
	lw	a3,%lo(i)(a5)
	lui	a5,%hi(i)
	lw	a4,%lo(i)(a5)
	lui	a5,%hi(f)
	slli	a3,a3,2
	addi	a5,a5,%lo(f)
	add	a5,a3,a5
	sw	a4,0(a5)
	lui	a5,%hi(i)
	lw	a5,%lo(i)(a5)
	addi	a4,a5,1
	lui	a5,%hi(i)
	sw	a4,%lo(i)(a5)
.L2:
	lui	a5,%hi(i)
	lw	a4,%lo(i)(a5)
	li	a5,3
	ble	a4,a5,.L3
	lui	a5,%hi(f)
	addi	a5,a5,%lo(f)
	lw	a4,8(a5)
	lui	a5,%hi(x)
	sw	a4,%lo(x)(a5)
	lui	a5,%hi(len)
	sw	zero,%lo(len)(a5)
	j	.L4
.L5:
	lui	a5,%hi(len)
	lw	a4,%lo(len)(a5)
	lui	a5,%hi(tmp)
	addi	a5,a5,%lo(tmp)
	add	a5,a4,a5
	li	a4,1
	sb	a4,0(a5)
	lui	a5,%hi(len)
	lw	a5,%lo(len)(a5)
	addi	a4,a5,1
	lui	a5,%hi(len)
	sw	a4,%lo(len)(a5)
	lui	a5,%hi(x)
	lw	a5,%lo(x)(a5)
	srai	a4,a5,1
	lui	a5,%hi(x)
	sw	a4,%lo(x)(a5)
.L4:
	lui	a5,%hi(x)
	lw	a5,%lo(x)(a5)
	bnez	a5,.L5
	lui	a5,%hi(i)
	sw	zero,%lo(i)(a5)
	j	.L6
.L7:
	lui	a5,%hi(i)
	lw	a4,%lo(i)(a5)
	lui	a5,%hi(top)
	addi	a5,a5,%lo(top)
	add	a5,a4,a5
	sb	zero,0(a5)
	lui	a5,%hi(i)
	lw	a5,%lo(i)(a5)
	addi	a4,a5,1
	lui	a5,%hi(i)
	sw	a4,%lo(i)(a5)
.L6:
	lui	a5,%hi(i)
	lw	a4,%lo(i)(a5)
	lui	a5,%hi(len)
	lw	a5,%lo(len)(a5)
	blt	a4,a5,.L7
	li	a5,0
	mv	a0,a5
	lw	s0,12(sp)
	addi	sp,sp,16
	jr	ra
	.size	main, .-main
	.ident	"GCC: (GNU) 7.2.0"
