all = []

for i in range(4):
    for j in range(7):
        xi = format(i, 'b').zfill(2)
        xj = format(j, 'b').zfill(3)
        print xi + ',' + xj + ': '
        str = ""
        name = raw_input()
        if (len(name)) > 0:
            str = "localparam OPCODE_%s = 7\'b%s%s11;"\
                % (name, xi, xj)
        else:
            str = "nothing"

        if (str != "nothing"):
            all.append(str)

print "========"

for item in all:
    print item
